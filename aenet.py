import lasagne
from lasagne.layers import InputLayer, MaxPool2DLayer, DenseLayer, NonlinearityLayer,batch_norm
from lasagne.layers import Conv2DLayer as ConvLayer

from lasagne.nonlinearities import softmax
import os
import htkfio
import ntpath
import numpy as np
import cPickle
import re

feat_shape=[3, 400, 50]

def build_model():
    '''
    Build Acoustic Event Net model
    :return:
    '''

    # # A architecture 41 classes
    # nonlin = lasagne.nonlinearities.rectify
    # net = {}
    # net['input'] = InputLayer((None, 3, 160, 50))   # channel, time. frequency
    # # ----------- 1st layer group ---------------
    # net['conv1a'] = batch_norm(ConvLayer(net['input'], num_filters=64, filter_size=(3,3), stride=1, nonlinearity=nonlin))
    # net['conv1b'] = batch_norm(ConvLayer(net['conv1a'], num_filters=64, filter_size=(3,3), stride=1, nonlinearity=nonlin))
    # net['pool1'] = MaxPool2DLayer(net['conv1b'], pool_size=(1,2))  # (time, freq)
    # # ----------- 2nd layer group ---------------
    # net['conv2a'] = batch_norm(ConvLayer(net['pool1'], num_filters=128, filter_size=(3,3), stride=1, nonlinearity=nonlin))
    # net['conv2b'] = batch_norm(ConvLayer(net['conv2a'], num_filters=128, filter_size=(3,3), stride=1, nonlinearity=nonlin))
    # net['pool2'] = MaxPool2DLayer(net['conv2b'], pool_size=(2,2))  # (time, freq)
    # # ----------- fully connected layer group ---------------
    # net['fc5'] = batch_norm(DenseLayer(net['pool2'],num_units=2048, nonlinearity=nonlin))
    # net['fc6'] = batch_norm(DenseLayer(net['fc5'],num_units=2048, nonlinearity=nonlin))
    # # net['fc7'] = batch_norm(DenseLayer(net['fc6'],num_units=41, nonlinearity=nonlin))
    # # net['fc8'] = batch_norm(DenseLayer(net['fc7'],num_units=41, nonlinearity=None))
    # net['fc7'] = DenseLayer(net['fc6'],num_units=41, nonlinearity=nonlin)
    # net['fc8'] = DenseLayer(net['fc7'],num_units=41, nonlinearity=None)
    # net['prob']  = NonlinearityLayer(net['fc8'], softmax)

    # B architecture
    nonlin = lasagne.nonlinearities.rectify
    net = {}
    net['input'] = InputLayer([None]+feat_shape)   # channel, time. frequency
    # ----------- 1st layer group ---------------
    net['conv1'] = ConvLayer(net['input'], num_filters=64, filter_size=(3,3), stride=1, nonlinearity=nonlin)
    net['conv2'] = ConvLayer(net['conv1'], num_filters=64, filter_size=(3,3), stride=1, nonlinearity=nonlin)
    net['pool1'] = MaxPool2DLayer(net['conv2'], pool_size=(1,2))  # (time, freq)
    # ----------- 2nd layer group ---------------
    net['conv3'] = ConvLayer(net['pool1'], num_filters=128, filter_size=(3,3), stride=1, nonlinearity=nonlin)
    net['conv4'] = ConvLayer(net['conv3'], num_filters=128, filter_size=(3,3), stride=1, nonlinearity=nonlin)
    net['pool2'] = MaxPool2DLayer(net['conv4'], pool_size=(2,2))  # (time, freq)
    # ----------- 3nd layer group ---------------
    net['conv5'] = ConvLayer(net['pool2'], num_filters=256, filter_size=(3,3), stride=1, nonlinearity=nonlin)
    net['conv6'] = ConvLayer(net['conv5'], num_filters=256, filter_size=(3,3), stride=1, nonlinearity=nonlin)
    net['pool3'] = MaxPool2DLayer(net['conv6'], pool_size=(2,1))  # (time, freq)

    # ----------- fully connected layer group ---------------
    net['fc7'] = DenseLayer(net['pool3'],num_units=2048, nonlinearity=nonlin)
    net['fc8'] = DenseLayer(net['fc7'],num_units=2048, nonlinearity=nonlin)
    net['fc9'] = DenseLayer(net['fc8'],num_units=28, nonlinearity=None)
    net['prob']  = NonlinearityLayer(net['fc9'], softmax)
    # net['prob'] = DenseLayer(net['fc8'],num_units=28, nonlinearity=softmax)
    return net


def set_weights(net,model_file):
    '''
    Sets the parameters of the model using the weights stored in model_file
    Parameters
    ----------
    net: a Lasagne layer

    model_file: string
        path to the model that containes the weights

    Returns
    -------
    None

    '''
    with open(model_file) as f:
        print('Load pretrained weights from %s ...' % model_file)
        model = cPickle.load(f)['param_values']
    print('Set the weights...')
    lasagne.layers.set_all_param_values(net, model,trainable=True)


def extract_fbank_htk(HCopyExe='./HCopy', configfile='configmfb.hcopy', scriptfile='hcopy.scp'):
    '''

    :param HCopyExe: path to HCopy executable file of HTK (http://htk.eng.cam.ac.uk/).
    :param configfile: path to the HCopy's configuration file
    :param scriptfile: path to the HCopy's script file
    :return: list of path to feature files
    '''

    with open(scriptfile,'r') as scpf:
        featfiles = scpf.readlines()
    featfiles = [f.split(' ')[1].replace('\n','') for f in featfiles]


    for f in featfiles:
        if not os.path.exists(ntpath.dirname(f)):
            os.makedirs(ntpath.dirname(f))

    cmd = HCopyExe+' -C '+configfile+' -S '+scriptfile
    os.system(cmd)

    return featfiles


def slice_data_gen(feat=None, datalen=100, outdatalen =160, slide=80, fdim=150):
    '''
    This function return slices of input feat slide
    :param feat: test featur vector
    :param datalen: length of test data
    :param outdatalen: length of output data
    :param slide:
    :param fdim:
    :return:
    '''
    # print 'datalen %d\toutdatalen %d\tslide %d' %(datalen,outdatalen,slide)
    outsampleN=int((datalen-outdatalen)/slide+1)

    td = np.empty((outsampleN,fdim*outdatalen),dtype=np.float32)
    data = feat.reshape(datalen,fdim)

    for i in range(outsampleN):
        tmpfeat=data[i*slide:i*slide+outdatalen]
        td[i] = tmpfeat.reshape(-1)

    return td



def get_feat_sequence(htkfile, slide, feat_mean, feat_std):
    '''
    :param htkfile: a feature file path
    :param slide: a window slide step. Normally 50% of input patch size.
    :param feat_mean: mean vector of features
    :param feat_std:  standard deviation vector of features
    :return: feature sequence
    '''
    feat = htkfio.open(htkfile, 'rb').getall()
    datalen = feat.shape[0]
    input_shape = [-1]+feat_shape

    # normalizaing
    feat = (feat - feat_mean[None,:])/feat_std[None,:]

    # if data length is shorter than target output length, repeat signal.
    if datalen < input_shape[2]:
       while datalen < input_shape[2]:
            # cpylen=min(outdatalen-datalen, datalen)
            cpylen=datalen
            feat = np.vstack((feat, feat[:cpylen]))  # repeat
            datalen = feat.shape[0]

    feats = slice_data_gen(feat, datalen, input_shape[2], slide, input_shape[1]*input_shape[3])
    feats = feats.reshape([input_shape[0],input_shape[2],input_shape[1],input_shape[3]])

    return np.swapaxes(feats,1,2)