# Author: Naoya Takahashi
# last modified: 8/6/2016

import sys, os
import aenet
import numpy as np
import lasagne
import theano
import cPickle
import ntpath

weight_file='./trained_param/net.pkl'

# Convert wave files to low-level feature files
feat_files = aenet.extract_fbank_htk()

# Build model
net = aenet.build_model()

# Set the pre-trained weights (takes some time)
aenet.set_weights(net['prob'],weight_file)

# Compile prediction function
prediction = lasagne.layers.get_output(net['prob'], deterministic=True)
pred_fn = theano.function([net['input'].input_var], prediction, allow_input_downcast=True);

# load the ClassName:index dictionary to display the detected event
classNumList = 'classNumDic.pickle'
with open(classNumList, 'r') as f:
    classNumList = cPickle.load(f)


# acoustic event class prediction
for f in feat_files:
    feat = aenet.get_feat_sequence(htkfile=f, slide=200, feat_mean=np.load('gmean.npy'), feat_std=np.load('gstd.npy'))

    # posterior probabilities for each time frame and class
    probabilities = pred_fn(feat)

    # we take the most probable index as a predicted class
    predicted_class = np.argmax(probabilities) % len(classNumList)
    print 'file: %s\tprediction:\t%s' % (f, classNumList[predicted_class])

print 'finished'