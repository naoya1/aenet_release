# Audio Event Net#

This README shows how to run AENet which classify the audio events.

### Requirements ###

* Python 2
* Lasagne (https://github.com/Lasagne/Lasagne)
* HCopy from HTK (http://htk.eng.cam.ac.uk/)

### How to use ###

* Install all requirements. (This repository include "HCopy" but you probably need to compile on your machine.)
* run "run_sample.py"
* The program read wave files listed in "copy.scp" and displays a classified event.

### Reference ###
If you end up using the code or parameter, we ask you to cite the following paper:
Naoya Takahashi, Michael Gygli, Beat Pfister and Luc Van Gool, " Deep Convolutional Neural Networks and Data Augmentation for Acoustic Event Recognition", Interspeech, pp.2982-2986, 2016